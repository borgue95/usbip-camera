#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 ip|all"
    exit -1
fi

username="orangepi"

if [ $1 == "all" ]; then
    ssh $username@cammans.local sudo shutdown now
    ssh $username@campeus.local sudo shutdown now
    ssh $username@camgeneral.local sudo shutdown now
else
    ssh $username@$1 sudo shutdown now
fi


