#!/bin/bash
# install usbip package

if [ ! -f /etc/modules-load.d/vhci-hcd.conf ]; then
	echo "vhci-hcd" | sudo tee -a /etc/modules-load.d/vhci-hcd.conf
fi
sudo modprobe vhci-hcd

# enable avahi discovery on your system
# create a pair of keys: ssh-keygen -t rsa
username="orangepi"

destination="cammans.local"
ssh $username@$destination mkdir -p .ssh
cat ~/.ssh/id_rsa.pub | ssh $username@$destination 'cat >> .ssh/authorized_keys'

destination="campeus.local"
ssh $username@$destination mkdir -p .ssh
cat ~/.ssh/id_rsa.pub | ssh $username@$destination 'cat >> .ssh/authorized_keys'

destination="camgeneral.local"
ssh $username@$destination mkdir -p .ssh
cat ~/.ssh/id_rsa.pub | ssh $username@$destination 'cat >> .ssh/authorized_keys'


