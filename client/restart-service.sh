#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 ip|all"
    exit -1
fi

username="orangepi"

if [ $1 == "all" ]; then
    ssh $username@cammans.local sudo systemctl restart usbip-webcam.service
    ssh $username@campeus.local sudo systemctl restart usbip-webcam.service
    ssh $username@camgeneral.local sudo systemctl restart usbip-webcam.service
else
    ssh $username@$1 sudo systemctl restart usbip-webcam.service
fi

