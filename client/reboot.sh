#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 ip|all"
    exit -1
fi

username="orangepi"

if [ $1 == "all" ]; then
    ssh $username@cammans.local sudo reboot
    ssh $username@campeus.local sudo reboot
    ssh $username@camgeneral.local sudo reboot
else
    ssh $username@$1 sudo reboot
fi


