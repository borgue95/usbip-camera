#!/bin/bash 

add_cam() {
    device=$(usbip list -r $1 | grep Logitech | sed 's/ //g' | cut -d: -f 1)
    if [ -n "$device" ]; then 
        sudo usbip attach -r $1 -b $device
    else
        echo "No device available in cammans.local"
    fi
}

if [ $# -ne 1 ]; then
	echo "Usage: $0 ip|all"
	exit -1
fi

if [ $1 == "all" ]; then
    # vhci driver needs a little bit of time to add the cameras. 3 seconds work great
    add_cam cammans.local
    sleep 3
    add_cam campeus.local
    sleep 3
    add_cam camgeneral.local
else
    add_cam $1
fi
