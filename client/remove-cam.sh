#!/bin/bash 

remove_cam() {
    port=$(sudo usbip port | grep -B 2 "$1" | head -n 1 | cut -d\  -f 2 | sed 's/://g')
    [ -n "$port" ] && sudo usbip detach -p $port
}

if [ $# -ne 1 ]; then
	echo "Usage: $0 ip|all"
	exit -1
fi

if [ $1 == "all" ]; then
    remove_cam cammans.local
    remove_cam campeus.local
    remove_cam camgeneral.local
else
    remove_cam "$1"
fi
