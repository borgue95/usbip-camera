#!/bin/bash
# install usbip package

if [ -z "$(grep -e "usbip-core" /etc/modules)" ]; then
	echo "usbip-core" | sudo tee -a /etc/modules
fi
if [ -z "$(grep -e "usbip-host" /etc/modules)" ]; then
	echo "usbip-host" | sudo tee -a /etc/modules
fi
sudo modprobe usbip-core
sudo modprobe usbip-host
sudo ln -sf $(pwd)/usbip-webcam.service /etc/systemd/system/usbip-webcam.service
sudo ln -sf $(pwd)/start.sh /usr/local/bin/usbip-webcam.sh
sudo systemctl enable --now usbip-webcam.service

